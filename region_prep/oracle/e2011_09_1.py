#!/usr/bin/python
# -*- coding: UTF-8 -*-

from random import normalvariate

L = 0.5
g = 9.8
n = -0.5
A = (14*L/3/g)**0.5

while True:
    try:
        s = float(raw_input("sin(alpha): "))
    except ValueError:
        print "Например: 0.5"
        continue
    if s > 1:
        print "Не бывает таких синусов"
        continue
    s = normalvariate(s, 0.001/L)
    try:
        t = A * s**n
    except ValueError:
        print "Шарик не скатился"
        continue
    t = normalvariate(t, 0.1)
    print "Время скатывания (с): %.2f" % (t)
