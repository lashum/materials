#!/usr/bin/python
# -*- coding: UTF-8 -*-

from random import normalvariate
from math import sqrt, exp


k = 1.37    # коэффициент пропорциональности
alpha = 1.1	# степень периметра 
H = 2				# высота броска
g = 9.8

print("Зависимость времени падения от периметра шарика")
for P in range(20, 100, 5):
    P = normalvariate(P,2);
    t = sqrt(2*H/g) + k * (P/100) ** (alpha)
    print("%i см : %.2f , %.2f , %.2f с " % 
    	(P, normalvariate(t,.1), normalvariate(t,.1),normalvariate(t,.1)))